package com.company.carrental.repository;

import com.company.carrental.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car, Integer> {

    //    @Query("select c from Car c where c.available = :available")
    List<Car> getCarByAvailable(boolean available);
}
