package com.company.carrental.controller;

import com.company.carrental.entity.Car;
import com.company.carrental.exceptions.CarNotAvailableException;
import com.company.carrental.exceptions.DoesNotExistException;
import com.company.carrental.repository.CarRepository;
import com.company.carrental.service.CarService;
import com.company.carrental.service.InitDb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CarController {

    @Autowired
    CarRepository carRepository;
    @Autowired
    InitDb initDb;
    @Autowired
    CarService carService;
    Logger logger = LoggerFactory.getLogger(CarController.class);


    @PostMapping("/testcar")
    public Boolean testCar() {
        
        try {
            Car car = new Car("Bosse", "volvo v70", 500.3);
            carRepository.save(car);
            initDb.initDb();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @PostMapping(value = "/addcar", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity addCar(@RequestBody Car car) {

        try {
            carService.addCar(car);
            logger.info(String.format("Admin created new car with id %s.", car.getId()));
            return new ResponseEntity<>("Car created", HttpStatus.OK);
        } catch (CarNotAvailableException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value = "/deletecar", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deletecar(@RequestBody Car car) {

        try {
            carService.deleteCar(car);
            logger.info(String.format("Admin made car with id %s inactive!", car.getId()));
            return new ResponseEntity<>("Car deleted", HttpStatus.OK);
        } catch (DoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/cars", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getCars() {

        List<Car> availableCars = carRepository.getCarByAvailable(true);
        if (availableCars.isEmpty())
            return new ResponseEntity<>("No available Cars", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>(availableCars, HttpStatus.OK);
    }

    @PutMapping(value = "/updatecar", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateCar(@RequestBody Car car) {

        try {
            carService.updateCar(car);
            logger.info(String.format("Admin updated car with id %s", car.getId()));
            return new ResponseEntity<>("Car has been updated", HttpStatus.OK);
        } catch (DoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
