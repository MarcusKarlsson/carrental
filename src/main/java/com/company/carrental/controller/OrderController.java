package com.company.carrental.controller;

import com.company.carrental.entity.Order;
import com.company.carrental.exceptions.CarNotAvailableException;
import com.company.carrental.exceptions.DoesNotExistException;
import com.company.carrental.repository.OrderRepository;
import com.company.carrental.service.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderController {


    @Autowired
    OrderRepository orderRepository;
    @Autowired
    OrderService orderService;

    Logger logger = LoggerFactory.getLogger(OrderController.class);

    @PostMapping(value = "/ordercar", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity makeOrder(@RequestBody Order order) {
        try {
            orderService.makeOrder(order);
            logger.info(String.format("Order %s has been made!", order.getId()));
            return new ResponseEntity<>("Order made", HttpStatus.OK);
        } catch (DoesNotExistException | CarNotAvailableException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping(value = "/myorders", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getOrders() {

        List<Order> orders = orderRepository.findAll();
        if (orders.isEmpty())
            return new ResponseEntity<>("No orders available.", HttpStatus.BAD_REQUEST);
        else
            return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @PutMapping(value = "/updateorder", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateOrder(@RequestBody Order order) {
        try {
            orderService.updateOrder(order);
            logger.info(String.format("Order %s has been canceled.", order.getId()));
            return new ResponseEntity<>("Order canceled.", HttpStatus.OK);
        } catch (DoesNotExistException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
