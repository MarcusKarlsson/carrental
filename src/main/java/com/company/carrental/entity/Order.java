package com.company.carrental.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id")
    private Car car;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    private Customer customer;
    private boolean active;
    @DateTimeFormat(iso = DateTimeFormat.ISO.NONE)
    private String pickupDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.NONE)
    private String endDate;
    private final LocalDateTime orderCreated = LocalDateTime.now();

    public Order() {
    }

    public Order(Car car, Customer customer, String pickupDate) {
        this.car = car;
        this.customer = customer;
        active = true;
        this.pickupDate = pickupDate;
    }

    public Integer getId() {
        return id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getOrderCreated() {
        return orderCreated;
    }
}
