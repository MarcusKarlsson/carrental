package com.company.carrental.service;

import com.company.carrental.entity.Car;
import com.company.carrental.entity.Customer;
import com.company.carrental.entity.Order;
import com.company.carrental.repository.CarRepository;
import com.company.carrental.repository.CustomerRepository;
import com.company.carrental.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitDb {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    CustomerRepository customerRepository;

    public void initDb() {
        Car car = new Car("Berit", "Volvo V70", 15.7);
        carRepository.save(car);
        Customer customer = new Customer("Klas klasson", "Hårdadress 1");
        customerRepository.save(customer);
        Order order = new Order(
                car, customer, "2012-01-02");
        order.setEndDate("2020-12-01");
        orderRepository.save(order);
    }
}
