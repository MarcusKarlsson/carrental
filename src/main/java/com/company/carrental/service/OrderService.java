package com.company.carrental.service;

import com.company.carrental.entity.Car;
import com.company.carrental.entity.Customer;
import com.company.carrental.entity.Order;
import com.company.carrental.exceptions.CarNotAvailableException;
import com.company.carrental.exceptions.DoesNotExistException;
import com.company.carrental.repository.CarRepository;
import com.company.carrental.repository.CustomerRepository;
import com.company.carrental.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    CustomerRepository customerRepository;

    public void makeOrder(Order order) {
        if (order.getCar() == null || order.getCustomer() == null) {
            throw new DoesNotExistException("Car or customer doesn't exist");
        }
        Optional<Car> optionalCar = carRepository.findById(order.getCar().getId());
        Optional<Customer> optionalCustomer = customerRepository.findById(order.getCustomer().getId());

        if (!optionalCar.isPresent() || !optionalCustomer.isPresent()) {
            throw new DoesNotExistException("Car or customer doesn't exist.");
        }
        order.setCar(optionalCar.get());
        order.setCustomer(optionalCustomer.get());
        if (!order.getCar().isAvailable() || !order.getCar().isActive()) {
            throw new CarNotAvailableException("Car is not available for rental.");
        } else {
            order.getCar().setAvailable(false);
            orderRepository.save(order);
        }
    }

    public void updateOrder(Order order) {
        Optional<Order> optionalOrder = orderRepository.findById(order.getId());

        if (!optionalOrder.isPresent()) {
            throw new DoesNotExistException("Order doesn't exist.");
        } else {
            order = optionalOrder.get();
            order.setActive(false);
            order.getCar().setAvailable(true);
            order.setEndDate("Canceled");
            orderRepository.save(order);
        }
    }
}
