package com.company.carrental.service;

import com.company.carrental.entity.Car;
import com.company.carrental.exceptions.CarNotAvailableException;
import com.company.carrental.exceptions.DoesNotExistException;
import com.company.carrental.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarService {

    @Autowired
    CarRepository carRepository;

    public void addCar(Car car) {
        Optional<Car> optionalCar = carRepository.findById(car.getId());
        if (optionalCar.isPresent()) {
            throw new CarNotAvailableException(
                    "Cannot add car. A car with that ID already exist.");
        } else {
            carRepository.save(car);
        }
    }

    public void deleteCar(Car car) {
        Optional<Car> optionalCar = carRepository.findById(car.getId());
        if (!optionalCar.isPresent()) {
            throw new DoesNotExistException(
                    "Cannot delete car. A car with that ID doesn't exist.");
        } else {
            car = optionalCar.get();
            car.setActive(false);
            car.setAvailable(false);
            carRepository.save(car);
        }
    }

    public void updateCar(Car car) {
        Optional<Car> optionalCar = carRepository.findById(car.getId());
        if (!optionalCar.isPresent()) {
            throw new DoesNotExistException(
                    "Cannot delete car. A car with that ID doesn't exist.");
        }

        Car existingCar = optionalCar.get();
        if (car.getDailyRate() <= 1)
            car.setDailyRate(car.getDailyRate());
        if (car.getName() == null)
            car.setName(existingCar.getName());
        if (car.getModel() == null)
            car.setModel(existingCar.getModel());
        car.setActive(true);
        car.setAvailable(true);
        carRepository.save(car);
    }
}



