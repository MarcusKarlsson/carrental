package com.company.carrental.exceptions;

public class DoesNotExistException extends RuntimeException {

    public DoesNotExistException(String errorMessage) {
        super(errorMessage);
    }
}
