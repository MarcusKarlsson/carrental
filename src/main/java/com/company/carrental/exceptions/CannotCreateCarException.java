package com.company.carrental.exceptions;

public class CannotCreateCarException extends RuntimeException {

    public CannotCreateCarException(String errorMessage) {
        super(errorMessage);
    }
}
