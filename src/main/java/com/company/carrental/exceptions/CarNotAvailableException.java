package com.company.carrental.exceptions;

public class CarNotAvailableException extends RuntimeException {

    public CarNotAvailableException(String errorMessage) {
        super(errorMessage);
    }
}
